// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// MARK: - Names

private enum Name {
  static let main = "ClIO"
  static let mainTests = "ClIOTests"
  static let xctAssertResult = "XCTAssertResult"
}

// MARK: - Dependencies

private enum Dep {
  
  enum Target {
    
    static let main = PackageDescription.Target.Dependency.init(stringLiteral: Name.main)
    static let assertResultPatch = PackageDescription.Target.Dependency.init(stringLiteral: Name.xctAssertResult)
  }
  
  enum Pkg {
    static let assertResultPatch = Package.Dependency.package(path: "../xctassert-result")
  }
}

// MARK: - Package

let package = Package(
  name: Name.main,
    products: [
      .library(name: Name.main, targets: [Name.main]),
    ],
    dependencies: [
      Dep.Pkg.assertResultPatch,
    ],
    targets: [
      .target(name: Name.main),
      .testTarget(
        name: Name.mainTests,
        dependencies: [
          Dep.Target.main,
          Dep.Target.assertResultPatch,
        ]
      )
    ]
)
