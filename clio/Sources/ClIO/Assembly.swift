//
//  Assembly.swift
//  cli
//
//  Created by Alex Apriamashvili on 08/02/2020.
//

import Foundation

public struct Assembly {
  
  public static func assembleInput<I: InputParameterKeyConstraint>(_ argv: [String],
                                                                   _ argc: Int32,
                                                                   optionType: I.Type) -> Input<I> {
    let loader = InputParameterLoader<I>.init(argv, argc)
    return Input<I>(loader)
  }
  
  public static func assembleOutput() -> CommandLineOutput {
    return Output()
  }
}
