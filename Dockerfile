## -- BUILD LAYER

# Swift image with swiftc compiler on-board
FROM swift:5.1.4-bionic as build
LABEL maintainer "Alex Apriamashvili"

ENV WORKSPACE_DIR="robo-tmp"
ENV PROJECT_ROOT="${WORKSPACE_DIR}/robot-simulator"
ENV BUILD_PATH="${PROJECT_ROOT}/.build/linux" \
	INSTRUCTIONS_ORIGIN="${PROJECT_ROOT}/instructions" \
	INSTRUCTIONS_DESTINATION="/instructions/" \
	BINARY_DESTINATION="/" \
	SHOWCASE_SCRIPT_ORIGIN="${PROJECT_ROOT}/run_all_examples.sh" \
	SHOWCASE_SCRIPT_DESTINATION="/"
ENV PATH_TO_RELEASE_BIN="${BUILD_PATH}/release/robosim"

COPY ./ "${WORKSPACE_DIR}"

RUN swift build -c release \
	--package-path "${PROJECT_ROOT}" \
	--build-path "${BUILD_PATH}"
RUN cp -R "${INSTRUCTIONS_ORIGIN}" "${INSTRUCTIONS_DESTINATION}"
RUN cp "${PATH_TO_RELEASE_BIN}" "${BINARY_DESTINATION}"
RUN cp "${SHOWCASE_SCRIPT_ORIGIN}" "${SHOWCASE_SCRIPT_DESTINATION}"
RUN rm -rf "${WORKSPACE_DIR}"

## -- EXECUTION LAYER

# Smaller swift image suitable for running
FROM swift:5.1.4-slim

COPY --from=build "/robosim" /usr/bin
COPY --from=build /instructions /instructions/
COPY --from=build /run_all_examples.sh /example.sh

CMD ["bash", "/example.sh", "./", "/usr/bin/robosim"]
