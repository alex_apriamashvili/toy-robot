VERSION 	?= 0.0.1
IMAGE_REPO 	?= alexapriamashvili/robosim

WORKSPACE_DIR	:= $(shell pwd)
WORKSPACE 		:= /robotemp
SWIFT_IMAGE		:= swift:5.1.4

ROBOSIM_PKG 		:= "./robot-simulator"
MACOSX_BUILD_DIR 	:= "./.build/macosx"
LINUX_BUILD_DIR		:= "./.build/linux"

all: build tag push

build:
	docker build -t $(IMAGE_REPO) .

tag:
	docker tag $(IMAGE_REPO) $(IMAGE_REPO):$(VERSION)

push:
	docker push $(IMAGE_REPO):$(VERSION)

build_macosx_release_and_install:
	swift build -c release \
		--build-path $(MACOSX_BUILD_DIR) \
		--package-path $(ROBOSIM_PKG)
	make install_on_macosx

install_on_macosx:
	mv "${MACOSX_BUILD_DIR}/release/robosim" "/usr/local/bin"

uninstall_on_macosx:
	rm -f "/usr/local/bin/robosim"


test_on_macosx:
	swift test \
		--build-path $(MACOSX_BUILD_DIR) \
		--package-path $(ROBOSIM_PKG)

test_on_linux:
	docker run --rm \
		--volume "${WORKSPACE_DIR}":"${WORKSPACE}" \
		--workdir "${WORKSPACE}" \
		"${SWIFT_IMAGE}" \
		/bin/bash -c \
		"swift test --build-path ${LINUX_BUILD_DIR} --package-path ${ROBOSIM_PKG}"