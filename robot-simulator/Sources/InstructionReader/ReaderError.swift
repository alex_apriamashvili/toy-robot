//
//  ReaderError.swift
//  InstructionReader
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import Foundation

extension ReaderError {
  
  public var localizedDescription: String {
    switch self {
    case let .cannotOpenFileAtPath(path):
      return "Cannot open file at path: \(path).\nPlease, make sure that file exists."
    case let .cannotReadContentsOfFile(path):
      return "Cannot read contents of file at path: \(path).\nPlease, make sure that instruction format is correct."
    }
  }
}
