//
//  Constant.swift
//  InstructionReader
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import Foundation

struct Constant {
  
  private init() {}
  
  static let readonlyFileOpenMode = "r"
  static let newlineSymbol = "\n"
}
