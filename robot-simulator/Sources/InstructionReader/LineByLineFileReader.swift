//
//  FileReader.swift
//  InstructionReader
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import Common
import Foundation

final class LineByLineFileReader: Reader {
  
  private let path: String
  private let file: UnsafeMutablePointer<FILE>
  
  init(_ path: String) throws {
    guard let file = fopen(path, Constant.readonlyFileOpenMode) else { throw ReaderError.cannotOpenFileAtPath(path) }
    self.path = path
    self.file = file
  }
  
  deinit {
    fclose(file)
  }
  
  func read() -> Result<[Instruction], ReaderError> {
    let contents = self.map { $0 }
    if contents.isEmpty { return .failure(.cannotReadContentsOfFile(path)) }
    let instructions = contents.compactMap { Instruction(rawValue: $0.dropNewline()) }
    return .success(instructions)
  }
}

extension LineByLineFileReader: Sequence {
  
  private var nextLine: String? {
    var line: UnsafeMutablePointer<CChar>? = nil
    var linecap = 0
    defer { free(line) }
    return getline(&line, &linecap, file) > 0 ? String(cString: line!) : nil
  }
  
  __consuming func makeIterator() -> AnyIterator<String> {
    return AnyIterator<String> { self.nextLine }
  }
}

private extension String {
  
  func dropNewline() -> String {
    return replacingOccurrences(of: Constant.newlineSymbol, with: "")
  }
}
