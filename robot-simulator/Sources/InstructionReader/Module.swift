//
//  Constants.swift
//  robosim
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Common

/// set of errors expected to be returned by a `Reader`
public enum ReaderError: Error {
  
  /// unable to open file at a given path
  case cannotOpenFileAtPath(String)
  
  /// unable to read file contents
  case cannotReadContentsOfFile(String)
}

public protocol Reader {
  
  /// read source and transform its contents into a list of instructions
  /// - returns a list of instructions if successful, otherwise, one of `ReaderError` errors
  func read() -> Result<[Common.Instruction], ReaderError>
}
