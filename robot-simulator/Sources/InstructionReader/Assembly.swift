//
//  Assembly.swift
//  InstructionReader
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import Foundation

public struct Assembly {
  
  public static func assemble(_ file: String) throws -> Reader {
    return try LineByLineFileReader(file)
  }
}
