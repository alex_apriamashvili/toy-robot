//
//  ToyRobot.swift
//  Common
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Common

final class ToyRobot: Simulatable {
  
  private var currentPosition: Position
  
  init(at position: Position) {
    currentPosition = position
  }
  
  func place(at position: Position) {
    currentPosition = position
  }
  
  func report() -> Position {
    return currentPosition
  }
  
  
  func move() {
    currentPosition = calculateNextPosition()
  }
  
  func turn(_ side: Side) {
    switch side {
    case .left:
      currentPosition = handleLeftRotation()
    case .right:
      currentPosition = handleRightRotation()
    }
  }
  
  func calculateNextPosition() -> Position {
    switch currentPosition.orientation {
    case .north:
      return Position(currentPosition.x, currentPosition.y + 1, currentPosition.orientation)
    case .south:
      return Position(currentPosition.x, currentPosition.y - 1, currentPosition.orientation)
    case .east:
      return Position(currentPosition.x + 1, currentPosition.y, currentPosition.orientation)
    case .west:
      return Position(currentPosition.x - 1, currentPosition.y, currentPosition.orientation)
    }
  }
}

private extension ToyRobot {
  
  func handleRightRotation() -> Position {
    switch currentPosition.orientation {
    case .north:
      return Position(currentPosition.x, currentPosition.y, .east)
    case .east:
      return Position(currentPosition.x, currentPosition.y, .south)
    case .south:
      return Position(currentPosition.x, currentPosition.y, .west)
    case .west:
      return Position(currentPosition.x, currentPosition.y, .north)
    }
  }
  
  func handleLeftRotation() -> Position {
    switch currentPosition.orientation {
    case .north:
      return Position(currentPosition.x, currentPosition.y, .west)
    case .west:
      return Position(currentPosition.x, currentPosition.y, .south)
    case .south:
      return Position(currentPosition.x, currentPosition.y, .east)
    case .east:
      return Position(currentPosition.x, currentPosition.y, .north)
    }
  }
}
