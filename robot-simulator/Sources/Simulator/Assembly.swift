//
//  Assembly.swift
//  Simulator
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Foundation

public struct Assembly {
  
  public static func assembleToyRobotAtSquareTabletopSimulator(width: Int,
                                                               height: Int) -> BidimensionalSimulator {
    let squareTabletop = SquareTabletop(width, height)
    return BoardSimulator(ToyRobot.self, squareTabletop)
  }
}
