//
//  Constants.swift
//  robosim
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Common

/// side in which that turn action can be specified
public enum Side {
  case right, left
}

/// a set of errors to be expected from a simulator
public enum SimulationError: Error, Equatable {
  
  /// empty instructions list was provided as input
  case noInstructionsProvided
  
  /// object was never initiated, hence never placed on a pilygon
  case objectWasNeverPlacedOnPolygon
}

/// a protocol that describes an entity that could be managed by a simulator
public protocol Simulatable {
  
  /// initialize an entity of the receiver with the given position on polygon
  /// - parameter position: initial position of simulatable on palygon
  init(at position: Common.Position)
  
  /// place entity at a given position
  /// - parameter position: position that the simulatable shall be placed at
  func place(at position: Common.Position)
  
  /// move receiver  in one space in the direction it's facing
  func move()
  
  /// turn receiver in direction
  /// - parameter _: direction that simulatable can be turned in (`left`|`right`)
  func turn(_: Side)
  
  /// report current position of the a receiver
  /// - returns: receiver's current position in polygon
  func report() -> Common.Position
  
  func calculateNextPosition() -> Position
}

/// a protocol that describes a bi-dimentional polygon, such as a square or a rectange
public protocol BidimensionalPolygon {
  
  typealias Coordinates = (x: Int, y: Int)
  
  /// initialise an entity of polygon with given dimensions
  /// - parameter x: width of the polygon
  /// - parameter y: height of the polygon
  init(_ x: Int, _ y: Int)
  
  /// ask receiver if an object can be placed at a given location
  /// - parameter position: intended location of an object
  /// - returns:`true` in case if placement is possible, otherwise `false`
  func canPlace(at position: Coordinates) -> Bool
}

/// a protocol that describes a simulator capable of performing simulations on bi-dimentional pilygons
public protocol BidimensionalSimulator {
  
  /// initialize the receiver with the given simulatable type and an entity of the a polygon
  /// - parameter _: type of simulatable to be initialized upon first successful placement
  /// - parameter _: an instance of a bi-dimensional polygon
  init<T: Simulatable>(_: T.Type, _: BidimensionalPolygon)
  
  /// execute a list of instructions given and return simulation result
  /// - parameter _: list of instructions to be simulated
  /// - returns: `.success` with nested result string in case if the simulation was successful,
  /// or `.failure` with its nested error if the simulation was not successful
  func execute(_: [Common.Instruction]) -> Result<String, SimulationError>
}
