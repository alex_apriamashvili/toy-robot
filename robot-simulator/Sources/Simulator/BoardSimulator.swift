//
//  BoardSimulator.swift
//  Simulator
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Common

final class BoardSimulator: BidimensionalSimulator {
  
  private var robot: Simulatable?
  
  private let robotType: Simulatable.Type
  private let board: BidimensionalPolygon
  
  init<T: Simulatable>(_ simulatable: T.Type, _ polygon: BidimensionalPolygon) {
    board = polygon
    robotType = simulatable
  }
  
  func execute(_ instructions: [Instruction]) -> Result<String, SimulationError> {
    guard !instructions.isEmpty else { return .failure(SimulationError.noInstructionsProvided) }
    var result: Result<String, SimulationError> = .success("")
    instructions.forEach {
      switch $0 {
      case .move:
        handleMove()
      case .left:
        robot?.turn(.left)
      case .right:
        robot?.turn(.right)
      case .report:
        result = reportRobotPosition()
        return
      case let .place(position):
        handlePlacement(position)
      }
    }
    return result
  }
}

private extension BoardSimulator {
  
  func handleMove() {
    guard isPossibleToMove() else { return }
    robot?.move()
  }
  
  func handlePlacement(_ position: Position) {
    let coordinates = positionToBoardCoordinates(position)
    guard board.canPlace(at: coordinates) else { return }
    if let bot = robot {
      bot.place(at: position)
    } else {
      robot = robotType.init(at: position)
    }
    
  }
  
  func isPossibleToMove() -> Bool {
    guard let estimatedPosition = robot?.calculateNextPosition() else { return false }
    let coordinates = positionToBoardCoordinates(estimatedPosition)
    return board.canPlace(at: coordinates)
  }
  
  func positionToBoardCoordinates(_ position: Position) -> BidimensionalPolygon.Coordinates {
    return (
      x: position.x,
      y: position.y
    )
  }
  
  func reportRobotPosition() -> Result<String, SimulationError> {
    guard let toyRobot = robot else { return .failure(SimulationError.objectWasNeverPlacedOnPolygon) }
    return .success(toyRobot.report().description)
  }
}
