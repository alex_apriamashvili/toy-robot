//
//  SimulationError.swift
//  Simulator
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import Foundation

extension SimulationError {
  
  public var localizedDescription: String {
    switch self {
    case .noInstructionsProvided:
      return "No instructions found in the input file. Please, ensure having correct instructions and try again."
    case .objectWasNeverPlacedOnPolygon:
      return "The object was never placed on the polygon. Please ensure having the correct 'PLACE' command among the instructions provided."
    }
  }
}
