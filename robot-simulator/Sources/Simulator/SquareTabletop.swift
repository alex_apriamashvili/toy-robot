//
//  Board.swift
//  Simulator
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Foundation

struct SquareTabletop: BidimensionalPolygon {
  
  private let xAxis: Int
  private let yAxis: Int
  
  init(_ x: Int, _ y: Int) {
    xAxis = x
    yAxis = y
  }
  
  func canPlace(at position: Coordinates) -> Bool {
    let isValidX = position.x >= 0 && position.x <= xAxis
    let isValidY = position.y >= 0 && position.y <= yAxis
    return isValidX && isValidY
  }
}
