//
//  Position.swift
//  ClIO
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

public struct Position: Equatable {
  
  public let x: Int
  public let y: Int
  public let orientation: Orientation
  
  public init(_ x: Int, _ y: Int, _ orientation: Orientation) {
    self.x = x
    self.y = y
    self.orientation = orientation
  }
  
  public var description: String {
    return "\(x),\(y),\(orientation.rawValue)"
  }
}
