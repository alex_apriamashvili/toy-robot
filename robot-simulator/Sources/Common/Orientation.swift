//
//  Orientation.swift
//  Common
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Foundation

public enum Orientation: String, Equatable {
  case north = "NORTH"
  case south = "SOUTH"
  case east = "EAST"
  case west = "WEST"
}
