//
//  Constants.swift
//  robosim
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Foundation

public enum Instruction {
  case place(Position)
  case move
  case left
  case right
  case report
}

extension Instruction: RawRepresentable, Equatable {
  
  public typealias RawValue = String
  
  public init?(rawValue: String) {
    switch rawValue {
    case CodingKeys.report:
      self = .report
    case CodingKeys.right:
      self = .right
    case CodingKeys.left:
      self = .left
    case CodingKeys.move:
      self = .move
    default:
      guard let position = Instruction.parsePosition(rawValue) else { return nil }
      self = position
    }
  }
  
  public var rawValue: String {
    switch self {
    case .left:
      return CodingKeys.left
    case .right:
      return CodingKeys.right
    case .move:
      return CodingKeys.move
    case .report:
      return CodingKeys.report
    case let .place(pos):
      return "\(CodingKeys.place) \(pos.description)"
    }
  }
}

private extension Instruction {
  
  static func parsePosition(_ rawPosition: String) -> Instruction? {
    let upercased = rawPosition.uppercased()
    guard upercased.hasPrefix(CodingKeys.place) else { return nil }
    let positionAttributes = upercased.replacingOccurrences(of: "\(CodingKeys.place) ", with: "")
    let attrs = positionAttributes.split(separator: ",").map(String.init)
    guard attrs.count == 3,
      let xPosition = Int(attrs[0]),
      let yPosition = Int(attrs[1]),
      let orientation = Orientation(rawValue: attrs[2]) else { return nil }
    
    return .place(Position(xPosition, yPosition, orientation))
  }
  
  enum CodingKeys {
    static let place = "PLACE"
    static let move = "MOVE"
    static let left = "LEFT"
    static let right = "RIGHT"
    static let report = "REPORT"
  }
}
