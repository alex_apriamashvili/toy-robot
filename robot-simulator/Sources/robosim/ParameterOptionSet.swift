//
//  ParameterOptionSet.swift
//  robosim
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import ClIO

enum ParameterOption: Equatable {
  case none
  case help
  case file
  case generic(Int)
}

extension ParameterOption: RawRepresentable, InputParameterKeyConstraint {
  
  var rawValue: String {
    switch self {
    case let .generic(number):
      return "\(number)"
    default:
      return "\(self)"
    }
  }
  
  static var empty: ParameterOption = .none
  
  init?(rawValue: String) {
    switch rawValue {
    case Opt.help.short, Opt.help.long:
      self = .help
    case Opt.file.short, Opt.file.long:
      self = .file
    default:
      guard let orderedNumber = Int(rawValue) else { return nil }
      self = .generic(orderedNumber)
    }
  }
  
  init?(value: String) {
    self.init(rawValue: value)
  }
  
  var value: String {
    return rawValue
  }
  
  struct Opt {
    
    let short: String
    let long: String
    
    static let help = Opt(short: "-h", long: "--help")
    static let file = Opt(short: "-f", long: "--file")
  }
}


