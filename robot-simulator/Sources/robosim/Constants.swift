//
//  Constants.swift
//  robosim
//
//  Created by Alex Apriamashvili on 15/02/2020.
//

import Foundation

enum Constant {
  
  static let boardWidthHeight = 5
  
  enum Message {
    
    static let availableParamsDescription = """
[-f, --file] [path to the file with instructions] – to specify a location of the file that contains instructions for the simulator.

-h, --help – to see the help message

"""
    
    static let help = """
(robosim)

=== DESCRIPTION ===

The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units. It is expected that there are no other obstructions on the table surface.
The robot is free to roam around the surface of the table, but it is prevented from falling to destruction. Any movement that would result in the robot falling from the table is prevented, however further valid movement commands are still allowed.


=== INPUT ===

Application can read input of the following form:
  PLACE X,Y,F
  MOVE
  LEFT
  RIGHT
  REPORT

The input is expected to be stored in file and the path to that file must be provided as an input parameter with or without option (-f, --file)

\(Constant.Message.availableParamsDescription)
    
=== OUTPUT ===

The application will output the terminal state of the robot including its position and direction.

Example: "0,1,NORTH"


"""
    
    static let noParameters = """
It seems that no parameters were provided. Please, specify one of the following parameters and try again.
    
\(Constant.Message.availableParamsDescription)
"""
    
    static let unrecognizedParameters = """
Input parameters were not recognized. Please, specify one of the following parameters and try again.
    
\(Constant.Message.availableParamsDescription)
"""
  }
  
  enum Code {
    
    static let success: Int32 = 0
    static let genericError: Int32 = 1
    static let unrecognizerParameters: Int32 = 2
  }
}
