
import ClIO
import Common
import Foundation
import InstructionReader
import Simulator

typealias InputParams = (key: ParameterOption, value: String)
typealias IOAssembly = ClIO.Assembly

let output = IOAssembly.assembleOutput()

final class CLI {
  
  public func main() -> Never {
    guard let input = loadParameters() else {
      output.writeMessage(Constant.Message.unrecognizedParameters, to: .error)
      exit(Constant.Code.unrecognizerParameters)
    }
    handleEarlyExitIfApplicable(input)
    let instructions = obtainInstructionList(input.value)
    return simulate(accordingTo: instructions)
  }
}

// MARK: - Private

private extension CLI {
  
  func loadParameters() -> InputParams? {
    return IOAssembly.assembleInput(
      CommandLine.arguments,
      CommandLine.argc,
      optionType: ParameterOption.self
    ).inputParameters.first
  }
  
  func handleEarlyExitIfApplicable(_ input: InputParams) {
    switch input.key {
    case .none, .empty:
      output.writeMessage(Constant.Message.noParameters, to: .error)
      exit(Constant.Code.unrecognizerParameters)
    case .help:
      output.writeMessage(Constant.Message.help)
      exit(Constant.Code.success)
    default:
      return
    }
  }
  
  func obtainInstructionList(_ file: String) -> [Instruction] {
    let instructions: [Instruction]
    do {
      let reader = try InstructionReader.Assembly.assemble(file)
      switch reader.read() {
      case let .success(instructionList):
        instructions = instructionList
      case let .failure(error):
        output.writeMessage(error.localizedDescription, to: .error)
        exit(Constant.Code.genericError)
      }
    } catch {
      output.writeMessage(error.localizedDescription, to: .error)
      exit(Constant.Code.genericError)
    }
    return instructions
  }
  
  func simulate(accordingTo instructions: [Instruction]) -> Never {
    let simulator = Simulator.Assembly.assembleToyRobotAtSquareTabletopSimulator(
      width: Constant.boardWidthHeight,
      height: Constant.boardWidthHeight
    )
    switch simulator.execute(instructions) {
    case let .success(position):
      output.writeMessage(position)
    case let .failure(error):
      output.writeMessage(error.localizedDescription, to: .error)
      exit(Constant.Code.genericError)
    }
    exit(Constant.Code.success)
  }
}

// MARK: – Execution Start Point

CLI().main()
