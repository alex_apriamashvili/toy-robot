import XCTest

import CommonTests
import SimulatorTests
import robosimTests

var tests = [XCTestCaseEntry]()
tests += CommonTests.__allTests()
tests += SimulatorTests.__allTests()
tests += robosimTests.__allTests()

XCTMain(tests)
