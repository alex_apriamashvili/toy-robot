import XCTest
@testable import Common

final class InstructionTests: XCTestCase {
    
  func testMoveInstructionIsRecognized() {
    let rawInstruction = "MOVE"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.move, result)
  }
  
  func testReportInstructionIsRecognized() {
    let rawInstruction = "REPORT"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.report, result)
  }
  
  func testLeftInstructionIsRecognized() {
    let rawInstruction = "LEFT"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.left, result)
  }
  
  func testRightInstructionIsRecognized() {
    let rawInstruction = "RIGHT"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.right, result)
  }
  
  func testZeroNorthPlacementIsRecognized() {
    let rawInstruction = "PLACE 0,0,NORTH"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.place(Position(0, 0, .north)), result)
  }
  
  func testWestPlacementIsRecognized() {
    let rawInstruction = "PLACE 1,5,WEST"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.place(Position(1, 5, .west)), result)
  }
  
  func testSouthPlacementIsRecognized() {
    let rawInstruction = "PLACE 5,5,SOUTH"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.place(Position(5, 5, .south)), result)
  }
  
  func testEastPlacementIsRecognized() {
    let rawInstruction = "PLACE 3,2,EAST"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.place(Position(3, 2, .east)), result)
  }
  
  func testLowercasedPlacementIsRecognized() {
    let rawInstruction = "place 3,2,east"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.place(Position(3, 2, .east)), result)
  }
  
  func testLowercasedOrientationPlacementIsRecognized() {
    let rawInstruction = "PLACE 3,2,east"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertEqual(Instruction.place(Position(3, 2, .east)), result)
  }
  
  func testPlacementWithTypoIsNotRecognized() {
    let rawInstruction = "PLASE 3,2,EAST"
    let result = Instruction(rawValue: rawInstruction)
    XCTAssertNil(result)
  }
}
