import XCTest
@testable import Common

final class PositionTests: XCTestCase {
    
  func testDescriptionNorthDescription() {
    let expected = "1,2,NORTH"
    let result = Position(1, 2, .north).description
    XCTAssertEqual(expected, result)
  }
  
  func testDescriptionEastDescription() {
    let expected = "1,2,EAST"
    let result = Position(1, 2, .east).description
    XCTAssertEqual(expected, result)
  }
  
  func testDescriptionWestDescription() {
    let expected = "1,2,WEST"
    let result = Position(1, 2, .west).description
    XCTAssertEqual(expected, result)
  }
  
  func testDescriptionSouthDescription() {
    let expected = "1,2,SOUTH"
    let result = Position(1, 2, .south).description
    XCTAssertEqual(expected, result)
  }
}
