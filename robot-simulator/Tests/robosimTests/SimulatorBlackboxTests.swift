
import Foundation
import XCTest

@available(macOS 10.13, *)
final class SimulatorBlackboxTests: XCTestCase {

  private var sutURL: URL!
  
  override func setUp() {
    super.setUp()
    sutURL = CLITestHelper.cliURL
  }
  
  override func tearDown() {
    sutURL = nil
    super.tearDown()
  }
  
  func testNoParamsDefinedExitCodeReturned() throws {
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: []) { code, _ in
      XCTAssertEqual(2, code)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
  
  func testLongHelpOptionRecognizedSuccessExitCode() throws {
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: ["--help"]) { code, _ in
      XCTAssertEqual(0, code)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
  
  func testShortHelpOptionRecognizedSuccessExitCode() throws {
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: ["-h"]) { code, _ in
      XCTAssertEqual(0, code)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
  
  func testUnrecognizedParametersExitCode() throws {
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: ["--path", "./some/path"]) { code, _ in
      XCTAssertEqual(2, code)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
  
  func testCorrectInstructionsLoaded() throws {
    let fixture = CLITestHelper.fixturesDir.appendingPathComponent("INSTRUCTIONS").relativePath
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: ["--file", "\(fixture)"]) { code, output in
      XCTAssertEqual(0, code)
      XCTAssertEqual("3,3,NORTH", output)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
  
  func testNonExistingFileParsingError() throws {
    let fixture = CLITestHelper.fixturesDir.appendingPathComponent("DO_NOT_EXIST").relativePath
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: ["--file", "\(fixture)"]) { code, _ in
      XCTAssertEqual(1, code)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
  
  func testEmptyInstructionsParsingError() throws {
    let fixture = CLITestHelper.fixturesDir.appendingPathComponent("INSTRUCTIONS_EMPTY").relativePath
    let expectation = XCTestExpectation()
    try CLITestHelper.runProcess(for: sutURL, parameters: ["--file", "\(fixture)"]) { code, _ in
      XCTAssertEqual(1, code)
      expectation.fulfill()
    }
    wait(for: [expectation], timeout: 0.5)
  }
}
