//
//  CLITestHelper.swift
//  robosimTests
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import Foundation

@available(macOS 10.13, *)
struct CLITestHelper {
  
  typealias ProcessTerminationHandler = (Int32, String) -> Void
  
  static var productsDirectory: URL {
    #if os(macOS)
      for bundle in Bundle.allBundles where bundle.bundlePath.hasSuffix(".xctest") {
        return bundle.bundleURL.deletingLastPathComponent()
      }
      fatalError("couldn't find the products directory")
    #else
      return Bundle.main.bundleURL
    #endif
  }
  
  static var cliURL: URL {
    productsDirectory.appendingPathComponent("robosim")
  }
  
  static func runProcess(for url: URL,
                         parameters: [String],
                         termination: @escaping ProcessTerminationHandler) throws {
    let process = Process()
    let pipe = Pipe()
    
    process.executableURL = url
    process.arguments = parameters
    process.standardOutput = pipe
    try process.run()
    process.waitUntilExit()
    
    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    let output = String(data: data, encoding: .utf8)?
      .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    termination(process.terminationStatus, output)
  }
  
  static var fixturesDir: URL {
    let thisSourceFile = URL(fileURLWithPath: #file)
    let levelUp = thisSourceFile.deletingLastPathComponent().deletingLastPathComponent()
    let fixturesURL = levelUp.appendingPathComponent("fixtures")
    return fixturesURL
  }
}
