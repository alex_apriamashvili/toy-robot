//
//  BoardSimulatorTests.swift
//  SimulatorTests
//
//  Created by Alex Apriamashvili on 16/02/2020.
//

import Common
import XCTest
@testable import Simulator

final class ToyRobotTests: XCTestCase {

  private var sut: ToyRobot!
  
  override func setUp() {
    super.setUp()
    let position = Position(0, 0, .north)
    sut = ToyRobot(at: position)
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  func testReport() {
    let result = sut.report()
    let expectedPosition = Position(0, 0, .north)
    XCTAssertEqual(expectedPosition, result)
  }
  
  func testPositionChange() {
    let position = Position(5, 7, .west)
    sut.place(at: position)
    let resultPosition = sut.report()
    
    XCTAssertEqual(position, resultPosition)
  }
  
  func testMove() {
    sut.move()
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 1, .north)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testCalculateNextStepTowardsNorth() {
    let result = sut.calculateNextPosition()
    let expectedPosition = Position(0, 1, .north)
    
    XCTAssertEqual(expectedPosition, result)
  }
  
  func testCalculateNextStepTowardsSouth() {
    let initialPosition = Position(1, 1, .south)
    sut = ToyRobot(at: initialPosition)
    let result = sut.calculateNextPosition()
    let expectedPosition = Position(1, 0, .south)
    
    XCTAssertEqual(expectedPosition, result)
  }
  
  func testCalculateNextStepTowardsWest() {
    let initialPosition = Position(1, 1, .west)
    sut = ToyRobot(at: initialPosition)
    let result = sut.calculateNextPosition()
    let expectedPosition = Position(0, 1, .west)
    
    XCTAssertEqual(expectedPosition, result)
  }
  
  func testCalculateNextStepTowardsEast() {
    let initialPosition = Position(1, 1, .east)
    sut = ToyRobot(at: initialPosition)
    let result = sut.calculateNextPosition()
    let expectedPosition = Position(2, 1, .east)
    
    XCTAssertEqual(expectedPosition, result)
  }
  
  func testLeftTurnFromFacingNorth() {
    let initialPosition = Position(0, 0, .north)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.left)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .west)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testLeftTurnFromFacingWest() {
    let initialPosition = Position(0, 0, .west)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.left)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .south)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testLeftTurnFromFacingSouth() {
    let initialPosition = Position(0, 0, .south)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.left)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .east)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testLeftTurnFromFacingEast() {
    let initialPosition = Position(0, 0, .east)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.left)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .north)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testRightTurnFromFacingNorth() {
    let initialPosition = Position(0, 0, .north)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.right)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .east)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testRightTurnFromFacingWest() {
    let initialPosition = Position(0, 0, .west)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.right)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .north)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testRightTurnFromFacingSouth() {
    let initialPosition = Position(0, 0, .south)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.right)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .west)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
  
  func testRightTurnFromFacingEast() {
    let initialPosition = Position(0, 0, .east)
    sut = ToyRobot(at: initialPosition)
    sut.turn(.right)
    let resultPosition = sut.report()
    let expectedPosition = Position(0, 0, .south)
    
    XCTAssertEqual(expectedPosition, resultPosition)
  }
}
