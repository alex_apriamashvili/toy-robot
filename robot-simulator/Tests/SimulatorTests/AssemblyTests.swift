//
//  AssemblyTests.swift
//  SimulatorTests
//
//  Created by Alex Apriamashvili on 22/02/2020.
//

import XCTest
@testable import Simulator

final class AssemblyTests: XCTestCase {

  typealias SUT = Simulator.Assembly
  
  func testAssembleCorrectToyRobotAtSquareTabletopSimulator() {
    let result = SUT.assembleToyRobotAtSquareTabletopSimulator(width: 5, height: 5)
    XCTAssert(result is BoardSimulator)
  }
}
