//
//  BoardSimulatorTests.swift
//  SimulatorTests
//
//  Created by Alex Apriamashvili on 16/02/2020.
//

import XCTest
@testable import Simulator
import Common
import XCTAssertResult

final class BoardSimulatorIntegrationTests: XCTestCase {

  private var sut: BoardSimulator!
  private var board: SquareTabletop!
  
  override func setUp() {
    super.setUp()
    board = SquareTabletop(5, 5)
    sut = BoardSimulator(ToyRobot.self, board)
  }

  override func tearDown() {
    board = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Basic
  
  func testPlaceMoveReportSequence() {
    let instructions: [Instruction] = [.place(Position(0,0,.north)), .move, .report]
    let expected = "0,1,NORTH"
    let result = sut.execute(instructions)
    XCTAssertResultEqual(expected, result)
  }
  
  func testPlaceLeftReportSequence() {
    let instructions: [Instruction] = [.place(Position(0,0,.north)), .left, .report]
    let expected = "0,0,WEST"
    let result = sut.execute(instructions)
    XCTAssertResultEqual(expected, result)
  }
  
  func testMultipleMoves() {
    let instructions: [Instruction] = [
      .place(Position(1, 2, .east)),
      .move,
      .move,
      .left,
      .move,
      .report
    ]
    let expected = "3,3,NORTH"
    let result = sut.execute(instructions)
    XCTAssertResultEqual(expected, result)
  }
  
  // MARK: - Falling off
  
  func testRobotDoesntFallOffTheBoard() {
    let instructions: [Instruction] = [
      .place(Position(5, 4, .north)),
      .move,
      .move,
      .report
    ]
    let expected = "5,5,NORTH"
    let result = sut.execute(instructions)
    XCTAssertResultEqual(expected, result)
  }
  
  // MARK: - Multiple Placements
  
  func testMultiplePlacements() {
    let instructions: [Instruction] = [
      .place(Position(0, 0, .north)),
      .move,
      .move,
      .left,
      .place(Position(0, 1, .south)),
      .move,
      .report
    ]
    let expected = "0,0,SOUTH"
    let result = sut.execute(instructions)
    XCTAssertResultEqual(expected, result)
  }
  
  func testIncorrectPlacementIsIgnored() {
    let instructions: [Instruction] = [
      .place(Position(0, 0, .north)),
      .move,
      .move,
      .right,
      .place(Position(2, 9, .south)),
      .move,
      .report
    ]
    let expected = "1,2,EAST"
    let result = sut.execute(instructions)
    XCTAssertResultEqual(expected, result)
  }
  
  // MARK: - Errors
  
  func testNoInstructionsProvidedError() {
    let result = sut.execute([])
    XCTAssertResultEqual(SimulationError.noInstructionsProvided, result)
  }
  
  func testRobotWasNeverPlacedError() {
    let instructions: [Instruction] = [.move, .move, .report]
    let result = sut.execute(instructions)
    XCTAssertResultEqual(SimulationError.objectWasNeverPlacedOnPolygon, result)
  }
  
  func testRobotCannotBePlacedError() {
    let instructions: [Instruction] = [.place(Position(6,6,.north)), .move, .report]
    let result = sut.execute(instructions)
    XCTAssertResultEqual(SimulationError.objectWasNeverPlacedOnPolygon, result)
  }
}
