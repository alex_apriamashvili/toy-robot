// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

private enum Name {
  
  static let main = "robosim"
  static let mainTests = "robosimTests"
  static let clio = "ClIO"
  static let sim = "Simulator"
  static let simTests = "SimulatorTests"
  static let common = "Common"
  static let commonTests = "CommonTests"
  static let assertResult = "XCTAssertResult"
  static let instructionReader = "InstructionReader"
}

private enum Dep {
  
  typealias TD = PackageDescription.Target.Dependency
  enum Target {
    static let clio = TD.init(stringLiteral: Name.clio)
    static let sim = TD.init(stringLiteral: Name.sim)
    static let common = TD.init(stringLiteral: Name.common)
    static let assertResult = TD.init(stringLiteral: Name.assertResult)
    static let instructionReader = TD.init(stringLiteral: Name.instructionReader)
  }
  
  typealias PD = PackageDescription.Package.Dependency
  enum Pkg {
    static let clio = PD.package(path: "../clio")
    static let assertResult = PD.package(path: "../xctassert-result")
  }
}

// MARK: - Targets (Prod)

let mainTarget = Target.target(
  name: Name.main,
  dependencies: [
    Dep.Target.clio,
    Dep.Target.sim,
    Dep.Target.common,
    Dep.Target.instructionReader,
  ]
)

let simTarget = Target.target(
  name: Name.sim,
  dependencies: [
    Dep.Target.common,
  ]
)

let instructionReaderTarget = Target.target(
  name: Name.instructionReader,
  dependencies: [
    Dep.Target.common,
  ]
)

let commonTarget = Target.target(name: Name.common)

// MARK: - Targets (Test)
let simTests = Target.testTarget(
  name: Name.simTests,
  dependencies: [
    Dep.Target.sim,
    Dep.Target.common,
    Dep.Target.assertResult,
  ]
)

let commonTests = Target.testTarget(
  name: Name.commonTests,
  dependencies: [
    Dep.Target.common,
  ]
)

let mainTests = Target.testTarget(name: Name.mainTests)

// MARK: - Package

let package = Package(
  name: Name.main,
    dependencies: [
      Dep.Pkg.clio,
      Dep.Pkg.assertResult,
    ],
    targets: [
      
      // - production
      mainTarget,
      commonTarget,
      simTarget,
      instructionReaderTarget,
      
      // - tests
      commonTests,
      simTests,
      mainTests,
    ]
)
