#! /bin/bash

__ROOT_DIR="${1:-./}"
__ROBOSIM_PATH="${2:-robosim}"
__EXAMPLES_DIR="${__ROOT_DIR}/instructions/"

__obtain_list_of_examples() {
	cd "${__EXAMPLES_DIR}" &> "/dev/null"
	ls | cut -f1 -d '/'
	cd - &> "/dev/null"
}

main() {
	local example_list=($(__obtain_list_of_examples))
	for example in "${example_list[@]}"; do
		local path_to_example="${__EXAMPLES_DIR}/${example}"
		echo -e "\n\nINSTRUCTION (${example}):"
		echo -e "=========="
		cat -e "${path_to_example}"
		echo -e "\n-----\nOUTPUT:"
		"${__ROBOSIM_PATH}" --file "${path_to_example}"
	done
}

main "${@}"