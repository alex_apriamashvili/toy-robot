# RoboSim

A simple cli tool written in `Swift`, to simulate robot actions on a square tabletop surface.

### Latest Version
The latest version at the moment is `0.0.1`. This version also corresponds to the version of the docker image.

## Usage
Currently, `robosim` supports only 2 options:
  * `-h`, `--help` – to show help message
  * `-f`, `--file` `[filepath]` – to provide set of instructions for one simulation

To perform simulation, simply use one of the following ways:

### Docker image
There is a docker image provided to demonstrate robot simulation. To execute showcase in the docker image, simply execute:
```bash
$ docker run --rm alexapriamashvili/robosim:<version>
```
By default, it will execute a set of showcase simulations and print output similar to:
```plain
...
INSTRUCTION (EXAMPLE_6_INCORRECT_PLACEMENT_IGNORED):
==========
PLACE 0,0,NORTH$
MOVE$
MOVE$
LEFT$
PLACE 2,9,SOUTH$
MOVE$
REPORT
-----
OUTPUT:
0,2,WEST
...
```
To run your own simulation, please, use a slightly different command, as you would have to mount your instructions on the docker container:
```bash
$ docker run --rm \
  --volume "<absolute_path_to_your_instructions_dir>":"/custom_instructions" \
  alexapriamashvili/robosim:<version>  \
  robosim --file "/custom_instructions/<your_instructions_filename>"
```

### macOS
If you are using `macOS` you might consider compiling the binary to run it in your system environment. To do that, please, follow these simple stems below:
1. **Ensure that you have `Swift Toolchain` installed on your machine**.  
This utility is written with `Swift 5.1`, so you need to have the same higher version of `swift` on your machine to ensure a smooth compilation process.
  * To see which version you currently have, execute: `swift --version` in terminal. If output gives you anything lower than `5.1.x` version, consider following the steps below.  
  * To download only swift toolchain, please, follow this [link](https://swift.org/builds/swift-5.1.4-release/xcode/swift-5.1.4-RELEASE/swift-5.1.4-RELEASE-osx.pkg)  
  * Once downloaded, run the installer to install the toolchain  
  * Make sure that `swift` was installed by executing `swift --version` in terminal. The output should print out version `5.1.4`.  
  ** Alternatively, you may simply update your Xcode on version `11.3`, which includes that version of `swift` by default.  
  2. **Compile your binary**.   
  As this cli client is written in Swift, a native Swift dependency manager called: `Swift Package Manager` or simply `SPM` was used to manage its dependencies. However, that is not just a dependency manager, but a real build system that allows you to compile and test your swift binaries. To compile `robosim` binary with SPM, please use `Makefile` located in the root directory. That `Makefile` contains instructions to compile and test your project of both macOS and Linux. The latter is possible by mounting sources on a swift docker container.  
  To perform compilation, simply execute:  
```bash
$ make build_macosx_release_and_install
```
That command will build a release version of `robosim` binary and install it under `/usr/local/bin`, so you can access to the cli more conveniently.    
_To uninstall `robosim`, simply execute: `make uninstall_from_macosx`_  
Now you can run your simulations.  

#### To run showcase simulation:  
Navigate to the `robot-simulator` directory and execute the following command in your terminal:  
```bash
$ bash run_all_examples.sh
```
#### To run custom simulations:  
To run your custom simulation, you just need to specify the path to the file that contains simulation instructions any of the ways listed below:  
```bash 
$ robosim -f /path/to/your/instructions_file
```
or, `-f | --file` can be omitted:  
```bash 
$ robosim /path/to/your/instructions_file
```
_To learn more about the format of that file, run `robosim --help`_  

## Testing

Currently, `robosim` has test coverage over `90%` for each of its internal targets. The coverage is distributed as the following:  
![coverage](https://bitbucket.org/alex_apriamashvili/toy-robot/raw/5ac11088e720a6290f5e24a6694a085ec1799d5f/coverage.png)

To run tests on macOS, simply execute:  
```bash
make test_on_macosx
```

To run tests on Linux, use the similar command:  
```bash
make test_on_linux
```
## Code
The application consists of several modules (targets). Some of these modules are internal, some are external.  
### External modules
External modules are not tightly connected to a certain application and can be reused between diferent projects. External modules used in this solution are listed below:  
* **ClIO** /`./clio`/ – a self-written library that allows to read input from the command line and transform it into a set of commands suitable for the consumer application  
* **XCTAssertResut** /`./xctassert-result`/ – a self-written test wrapper that helps to assert swift `Result<S,E>` types more conveniently  
### Internal modules
/`./robot-simulator`/  
Internal modules are the modules that belong to a certain application (`robosim`) and solve particular problems that the application might have. Having multiple modules helps to facilitate proper segregation of concerns and distribute the load on different CPUs, which makes a compilation of a resulting binary faster. Internal modules used in this application are listed below:  
* **robosim** – application target (executable). It contains the high-level business logic of the application. Serves as an entry point to the rest of the application.  
* **Simulator** – a module responsible for simulation logic. Receives instruction as input and produces a result as an output.  
* **InstructionReader** – a module that provides instructions for the previous module, by mapping contents of the provided source file into the set of instructions available for simulation  
* **Common** – a module that holds information that is used by both `Simulator` and `InstructionReader` modules.

