# XCTAssertResult

A tiny helper-library that serves as an addition to the `XCTest` framework and provides a handy way to create assertions for `Result<Success, Failure>` types in your swift test-cases.   
